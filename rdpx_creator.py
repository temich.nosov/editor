from lxml import etree
import lxml.builder as builder

ITEM_CODE = -1
LECTURE = 101
LAB_WORK = 102
INDIVIDUAL = 107
PRACTICE = 103
EXAM = 1
TEST = 1
COURSE_PROJ = 4
COURSE_WORK = 5
CONTROL_WORK = 6
CONTACT_WORK = 143


class ContentBuilder:
    built: "list"
    id: int

    def __init__(self):
        self.built = []
        self.id = 0
        self.cur_item_id = -1
        self.cur_item_num = 0
        self.E = builder.ElementMaker(namespace="http://tempuri.org/RPDataSet.xsd", nsmap={
            None: "http://tempuri.org/RPDataSet.xsd",
            "diffgr": "urn:schemas-microsoft-com:xml-diffgram-v1",
            "msdata": "urn:schemas-microsoft-com:xml-msdata"
        })

    def create_rp_work(self):
        root = self.E.rpWorks()

        root.attrib["{urn:schemas-microsoft-com:xml-diffgram-v1}id"] = "rpWorks" + str(self.id + 1)
        root.attrib["{urn:schemas-microsoft-com:xml-diffgram-v1}hasChanges"] = "inserted"
        root.attrib["{urn:schemas-microsoft-com:xml-msdata}rowOrder"] = str(self.id)

        return root

    def add_theme(self, name):
        root = self.create_rp_work()
        pass

    def add_lecture(self, name, hours):
        pass

#   def create_work(self, E, type_id, rup_type_id):
    def create_work(self, E, work):
        root = E.rpWorks()

        root.attrib["{urn:schemas-microsoft-com:xml-diffgram-v1}id"] = "rpWorks" + str(self.id + 1)
        root.attrib["{urn:schemas-microsoft-com:xml-diffgram-v1}hasChanges"] = "inserted"
        root.attrib["{urn:schemas-microsoft-com:xml-msdata}rowOrder"] = str(self.id)

        #   <workId>-15</workId>
        #   <partId>-2</partId>
        #   <rowNum>9</rowNum>
        #   <name>Контактная работа с ППС</name>
        #   <semester>5</semester>
        #   <hours>0.35</hours>
        #   <workTypeId>-29</workTypeId>
        #   <comment>Э</comment>
        #   <parentId>-13</parentId>
        #   <rupWorkTypeId>143</rupWorkTypeId>

        # root.append(E.workId(str(-1 - self.id)))
        # root.append(E.partId("-1"))
        # root.append(E.rowNum("1"))
        # root.append(E.name("Пример названия + " + str(self.id)))
        # root.append(E.semester("5"))
        # root.append(E.hours("8"))
        # root.append(E.comment("Hello, world"))
        # root.append(E.parentId("-1"))
        # root.append(E.workTypeId(str(type_id)))
        # root.append(E.rupWorkTypeId(str(rup_type_id)))

        root.append(E.workId(str(-1 - self.id)))
        root.append(E.partId(str(work.part)))
        if work.type == -1:
            self.cur_item_id = -1 - self.id
            self.cur_item_num += 1
            root.append(E.rowNum(str(self.cur_item_num)))
        else:
            # пока так, потом нужно сделать лучше
            root.append(E.rowNum(str(work.order)))
#       ?!!!
#       root.append(E.rowNum("1"))
        root.append(E.name(work.name))
        root.append(E.semester(work.semester))
        if work.type != -1:
            root.append(E.hours(str(work.hours)))
            root.append(E.parentId(str(self.cur_item_id)))
            root.append(E.comment(work.comment))
        root.append(E.workTypeId(str(work.type)))
        root.append(E.rupWorkTypeId(str(work.rup_type)))

        return root

    def to_xml(self, work_list, rpd_id):
        E = builder.ElementMaker(namespace="http://tempuri.org/RPDataSet.xsd", nsmap={
                    None: "http://tempuri.org/RPDataSet.xsd",
                    "diffgr": "urn:schemas-microsoft-com:xml-diffgram-v1",
                    "msdata": "urn:schemas-microsoft-com:xml-msdata"
        })

        res = []

        part1 = E.rpParts()
        part1.attrib["{urn:schemas-microsoft-com:xml-diffgram-v1}id"] = "rpParts1"
        part1.attrib["{urn:schemas-microsoft-com:xml-diffgram-v1}hasChanges"] = "inserted"
        part1.attrib["{urn:schemas-microsoft-com:xml-msdata}rowOrder"] = "0"
        part1.append(E.partId("-1"))
        part1.append(E.rpdId(rpd_id))
        # part1.append(E.rpdId("18846"))
        part1.append(E.rowNum("1"))
        part1.append(E.name("Обучение"))
        part1.append(E.newModule("false"))

        res.append(part1)

        part2 = E.rpParts()
        part2.attrib["{urn:schemas-microsoft-com:xml-diffgram-v1}id"] = "rpParts2"
        part2.attrib["{urn:schemas-microsoft-com:xml-diffgram-v1}hasChanges"] = "inserted"
        part2.attrib["{urn:schemas-microsoft-com:xml-msdata}rowOrder"] = "1"
        part2.append(E.partId("-2"))
        part2.append(E.rpdId(rpd_id))
        # part2.append(E.rpdId("18846"))
        part2.append(E.rowNum("2"))
        part2.append(E.name("Промежуточная аттестация"))
        part2.append(E.newModule("false"))

        res.append(part2)
        # root.append(E.rowNum("1"))
        # root.append(E.name("Пример названия"))
        # root.append(E.semester("5"))
        # root.append(E.workTypeId("-1"))
        # root.append(E.rupWorkTypeId("-1"))

        root = E.rpWorks()

        # root.attrib["{urn:schemas-microsoft-com:xml-diffgram-v1}id"] = "rpWorks" + str(self.id + 1)
        # root.attrib["{urn:schemas-microsoft-com:xml-diffgram-v1}hasChanges"] = "inserted"
        # root.attrib["{urn:schemas-microsoft-com:xml-msdata}rowOrder"] = str(self.id)

        # root.append(E.workId(str(-1 - self.id)))
        # root.append(E.partId("-1"))
        # root.append(E.rowNum("1"))
        # root.append(E.name("Пример названия"))
        # root.append(E.semester("5"))
        # root.append(E.workTypeId("-1"))
        # root.append(E.rupWorkTypeId("-1"))

        # self.id = self.id + 1
        # res.append(self.create_work(E, 8, 101))
        # self.id = self.id + 1
        # res.append(self.create_work(E, 1, 143))
        # self.id = self.id + 1

        for w in work_list:
            res.append(self.create_work(E, w))
            self.id += 1

        #res = [root]
        return res

    # print(etree.tounicode(root, pretty_print=True))
    
    def built(self):
        return self.built


class RdpxDocument:
    template: etree

    def __init__(self, template: etree) -> None:
        self.template = template

    def change_works(self, work_list):
        titles = self.template.getroot().findall('.//{http://tempuri.org/RPDataSet.xsd}rpTitle')
        p = titles[0].getparent()

        title = self.template.getroot().findall('.//{http://tempuri.org/RPDataSet.xsd}rpTitle')

        # for x in rpd_id[0]:
        #     print(x.text)

        # ?!!
        rpd_id = title[0][1].text

        # for x in works:
        #     print(etree.tounicode(x, pretty_print=True))
        #     pass

        works = self.template.getroot().findall('.//{http://tempuri.org/RPDataSet.xsd}rpWorks')
        for w in works:
            p.remove(w)

        for x in ContentBuilder().to_xml(work_list, rpd_id):
            print(etree.tounicode(x, pretty_print=True))
            p.append(x)

    def render(self, file: str) -> None:
        self.template.write_c14n(file)