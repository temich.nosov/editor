from collections import defaultdict
import docx
from itertools import dropwhile
import re
from dataclasses import dataclass

from document_iterator import get_all_tables_with_header_text
from topic import WorkItem, Topic


def extract_name_from_text(text):
    # учебной дисциплины «Мобильные и встраиваемые операционные системы»
    m = re.search('.*«(.*)»', text)
    return m.group(1)


def get_from_dict_with_assert(dict, key):
    assert key in dict
    return dict[key]


@dataclass
class HeaderWithText:
    header: str
    text: "list[docx.Paragraph]"


def paragraph_is_header(p) -> bool:
    return p.style.name == "Heading 1"


def aggregate_text(paragraphs) -> str:
    return "\n".join([p.text for p in paragraphs])


class ParsedDocxDocument:
    document: docx.Document

    def __init__(self, filename: str) -> None:
        self.document = docx.Document(filename)

    def print_paragraphs(self):
        print(len(self.document.paragraphs))
        for idx, x in enumerate(self.document.paragraphs):
            print("=== === " + str(idx) + " === ===")
            print(x.text)

    def get_discipline_name(self) -> str:
        return get_from_dict_with_assert(self.get_passport_dict(), "Дисциплина:")

    def get_passport_table(self):
        # print(self.document.tables)
        return self.document.tables[0]  # Tmp solution

    def get_passport_dict(self):
        table = self.get_passport_table()
        res = {}
        for row in table.rows:
            assert len(row.cells) == 2
            res[row.cells[0].text] = row.cells[1].text
        return res

    def get_laboriousness(self) -> str:
        return get_from_dict_with_assert(self.get_passport_dict(), "Число зачетных единиц трудоемкости:")

    def get_hours(self) -> str:
        return get_from_dict_with_assert(self.get_passport_dict(), "Всего часов по учебному плану:")

    def get_text_with_headers(self) -> "list[HeaderWithText]":
        res = []
        for p in self.document.paragraphs:
            if paragraph_is_header(p) and p.text != "" and not p.text.isspace():
                res.append(HeaderWithText(p.text, []))
            else:
                if len(res) == 0:
                    res.append(HeaderWithText("", []))
                res[-1].text.append(p)
        return res

    def get_goals(self) -> str:
        text_with_headers = self.get_text_with_headers()
        matches = [x for x in text_with_headers if "цели и задачи освоения дисциплины" in x.header.lower()]
        assert len(matches) == 1
        return aggregate_text(matches[0].text)

    def get_tables_with_names(self) -> "dict[int, list[list[str]]]":
        res = defaultdict(list)
        for k, v in get_all_tables_with_header_text(self.document):
            res[k] = v
        return res

    def get_semesters(self) -> "list[int]":
        return [int(s) for s in str(self.get_passport_dict()["Семестр обучения:"]).split()]

    def get_ctrl(self) -> "list[str]":
        return [s for s in self.get_passport_dict()["Форма итогового контроля по дисциплине:"].split()[0]]

def empty_str(s):
    return s == "" or s.isspace()


# @dataclass
# class Lection:
#     title: str
#     hours: float
#     comments: str
#
#
# @dataclass
# class LabWork:
#     title: str
#     hours: float
#     comments: str
#
#
# @dataclass
# class Seminar:
#     title: str
#     hours: float
#     comments: str
#
#
# @dataclass
# class ControlWork:
#     title: str
#     hours: float
#     type: int
#
#
# @dataclass
# class IndividualWork:
#     title: str
#     hours: float
#     comments: str


def process_works(hours, labs, seminars, srs, sem, ctrl):
    #print("Раздел 1")
    parts = []
    hours = hours[3:-1]
    labs = labs[2:-1]
    seminars = seminars[2:-1]
    viewed_labs = 0
    viewed_sem = 0
    # Взять  откуда ?!!
    # TODO !
    semester = sem

    for row in hours:
        if row[1] == '2':
            continue
        theme_title = "Тема 1." + str(row[0])
        topic = Topic([], [], [], [], [], [], [], [], [], [])
        # topic_title = upper_part_or_just_1st(row[1])
        topic.lections.append(WorkItem(row[1], row[2], semester, row[7]))
        lab_hours = 0 if empty_str(row[4]) or (not row[4].isnumeric()) else int(row[4])
        while lab_hours > 0:
            cur_lab = labs[viewed_labs]
            lab_hours = lab_hours - int(cur_lab[2])
            topic.lab_works.append(WorkItem(cur_lab[1], cur_lab[2], semester, "Ko"))
            viewed_labs = viewed_labs + 1

        if lab_hours < 0 and (viewed_labs > 0) and (labs[viewed_labs - 1][1].find('тчетное') < 0):
            viewed_labs -= 1

        # Практики - нужен рефакторинг (повтор !)
        sem_hours = 0 if empty_str(row[3]) or (not row[3].isnumeric()) else int(row[3])
        while sem_hours > 0:
            cur_sem = seminars[viewed_sem]
            sem_hours = sem_hours - int(cur_sem[2])
            topic.seminars.append(WorkItem(cur_sem[1], cur_sem[2], semester, "К,Ko"))
            viewed_sem = viewed_sem + 1

        if sem_hours < 0 and (viewed_sem > 0) :
            viewed_sem -= 1

        sr_title = "Подготовка к выполнению заданий по контрольной работе, подготовка к текущему контролю успеваемости"
        topic.individual_works.append(WorkItem(sr_title, row[6], semester, "K,Ko"))
        # print(theme_title)
        # print(lection_title)
        # print(lab_titles)
        # print(sr_title)
        # print(topic)
        parts.append(topic)

    # print(parts)

    # TODO: Добавить контрольную работу
    topic = Topic([], [], [], [], [], [], [], [], [], [])
    #if str(srs[0]).find('онтрольная') >= 0:
    topic.control_works.append(WorkItem(srs[2][0], float(srs[2][3]), semester, ''))
    parts.append(topic)

    # TODO: Добавить раздел 2 (там и контрольные ? - нет !..)
    topic = Topic([], [], [], [], [], [], [], [], [], [])
    if ctrl.lower().find('э') >= 0:
        topic.exams.append(WorkItem("Экзамен", 0.0, semester, ''))

    if ctrl.lower().find('з') >= 0:
        topic.exams.append(WorkItem("Зачет", 0.0, semester, ''))

    parts.append(topic)
    return parts


def process_works_from_all_tables(all_tables, sem, ctrl):
    hours = None
    labs = None
    seminars = None
    srs = None
    for key, value in all_tables.items():
        if "таблица д2 – " in key.lower():
            hours = value
        if "таблица д3 – " in key.lower():
            labs = value
        if "таблица д4 – " in key.lower():
            seminars = value
        if "таблица д5 – " in key.lower():
            srs = value
    assert hours is not None
    assert labs is not None
    assert seminars is not None
    assert srs is not None

    return process_works(hours, labs, seminars, srs, sem, ctrl)


def play(filename):
    parsed_document = ParsedDocxDocument(filename)

    tables = parsed_document.get_tables_with_names()
    # # print(tables)
    # for key, value in tables.items():
    #     print("\n\n")
    #     print(key)
    #     print("-----------")
    #     for r in value:
    #         print(r)
    #     # print(value)

    process_works_from_all_tables(tables, 5, '')


def get_topics_from_docx(filename):
    parsed_document = ParsedDocxDocument(filename)
    tables = parsed_document.get_tables_with_names()
    sem = parsed_document.get_semesters()[0]
    ctrl = parsed_document.get_ctrl()[0]
    return process_works_from_all_tables(tables, sem, ctrl)
