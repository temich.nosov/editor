def uppercase_part(text: str) -> str:
    first_upper = ''
    parts = text.split('.')
    i = 0
    while parts[i].isupper():
        first_upper += parts[i] + "."
        i += 1
    return first_upper


def upper_and_rest(text: str):
    parts = []
    part = uppercase_part(text)
    if len(part) == 0:
        part = text.split('.')[0]
    parts.append(part.upper())
    parts.append(text.replace(part, '').lstrip(' .'))
    return parts
