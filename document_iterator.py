from docx.document import Document
from docx.oxml.table import CT_Tbl
from docx.oxml.text.paragraph import CT_P
from docx.table import _Cell, Table
from docx.text.paragraph import Paragraph

def iter_block_items(parent):
    # Get parrent element
    if isinstance(parent, Document):
        parent_elm = parent.element.body
    elif isinstance(parent, _Cell):
        parent_elm = parent._tc
    else:
        raise ValueError("something's not right")

    for child in parent_elm.iterchildren():
        if isinstance(child, CT_P):
            yield Paragraph(child, parent)
        elif isinstance(child, CT_Tbl):
            yield Table(child, parent)

def empty_str(s):
    return s == "" or s.isspace()

def is_continuation(s):
    return "продолжение таблицы" in s.lower()

def extract_table(table):
    res = []
    for row in table.rows:
        res.append([x.text for x in row.cells])
    return res

def concat_tables(first, second):
    # First two rows can be headers
    header_lines = 0
    while header_lines < 2 and len(second) > header_lines and len(first) > header_lines and first[header_lines] == second[header_lines]:
        header_lines += 1
    return first + second[header_lines:]

def get_all_tables_with_header_text(doc):
    rlist = [x for x in iter_block_items(doc)]
    tables_with_header = []
    for i in range(len(rlist)):
        if isinstance(rlist[i], Table):
            j = i - 1
            while j >= 0 and not isinstance(rlist[j], Paragraph) or empty_str(rlist[j].text):
                j = j - 1
            if len(tables_with_header) != 0 and is_continuation(rlist[j].text):
                header, table = tables_with_header[-1]
                tables_with_header[-1] = (header, concat_tables(table, extract_table(rlist[i])))
            else:
                tables_with_header.append((rlist[j].text, extract_table(rlist[i])))
    return tables_with_header