﻿import argparse
from lxml import etree
import docx

from rdpx_creator import ITEM_CODE, INDIVIDUAL, LECTURE, LAB_WORK
from rdpx_creator import PRACTICE, COURSE_WORK, COURSE_PROJ, CONTACT_WORK
from rdpx_creator import CONTROL_WORK, EXAM
from rdpx_creator import RdpxDocument
from parsed_docx import play, get_topics_from_docx
from work import Work
from topics_creator import create_works_from_topics


def docx_const_works():
    parser = argparse.ArgumentParser(description='Parser')
    parser.add_argument('--filename', dest='filename', required=True)
    parser.add_argument('--rdpx-filename', dest='rdpx_filename', required=True)
    parser.add_argument('--res-filename', dest='res_filename', required=True)
    args = parser.parse_args()
    template = etree.parse(args.rdpx_filename)
    document = RdpxDocument(template)
    # символические имена ?... => сделал, в этом примере практически все основные коды есть
    work_list = [Work(-1, ITEM_CODE, ITEM_CODE, "Название темы 1", "5", 0, ""),
                 Work(-1, 1, LECTURE, "Название темы лекции 1", "5", 8, "Эк, К"),
                 Work(-1, 1, LAB_WORK, "Название лабораторной 1", "5", 4, "К"),
                 Work(-1, 1, PRACTICE, "Название практики 1", "5", 4, "К"),
                 Work(-1, 1, INDIVIDUAL,
                      "Подготовка к выполнению заданий по контрольной работе, подготовка к текущему контролю "
                      "успеваемости",
                      "5", 16, "Эк, К"),
                 Work(-1, ITEM_CODE, ITEM_CODE, "Название темы 2", "5", 0, ""),
                 # с parent нужно что-то придумывать !!! С нумерацией !!! Пока подряд
                 Work(-1, 1, LECTURE, "Название темы лекции 2", "5", 8, "Эк, К"),
                 Work(-1, 1, LAB_WORK, "Название лабораторной 2", "5", 4, "К"),
                 Work(-1, 1, PRACTICE, "Название практики 2", "5", 4, "К"),
                 Work(-1, 1, INDIVIDUAL,
                      "Подготовка к выполнению заданий по контрольной работе, подготовка к текущему контролю "
                      "успеваемости",
                      "5", 16, "Эк, К"),
                 Work(-1, ITEM_CODE, ITEM_CODE, "Контрольная работа", "5", 0, ""),
                 Work(-1, 1, CONTROL_WORK, "Контрольная работа", "5", 4, "Ко"),
                 Work(-1, ITEM_CODE, ITEM_CODE, "Курсовая работа", "5", 0, ""),
                 Work(-1, 1, COURSE_WORK, "Курсовая работа", "5", 4, "Ко"),
                 Work(-1, ITEM_CODE, ITEM_CODE, "Курсовой проект", "5", 0, ""),
                 Work(-1, 1, COURSE_PROJ, "Курсовой проект", "5", 4, "Ко"),
                 Work(-2, ITEM_CODE, ITEM_CODE, "Экзамен", "5", 0, ""),
                 Work(-2, 1, EXAM, "Подготовка к экзамену", "5", 35.65, "Э"),
                 Work(-2, 1, CONTACT_WORK, "Контактная работа с ППС", "5", 0.35, "Э")
                 ]
    document.change_works(work_list)
    document.render(args.res_filename)

    # play(args.filename)


# Ну а тут попытка загрузить автоматически все работы
# из docx и вставить их в шаблон
def works_from_docx():
    parser = argparse.ArgumentParser(description='Parser')
    parser.add_argument('--filename', dest='filename', required=True)
    parser.add_argument('--rdpx-filename', dest='rdpx_filename', required=True)
    parser.add_argument('--res-filename', dest='res_filename', required=True)
    args = parser.parse_args()
    template = etree.parse(args.rdpx_filename)
    document = RdpxDocument(template)
    # символические имена ?... => сделал, в этом примере практически все основные коды есть
    work_list = create_works_from_topics(get_topics_from_docx(args.filename))
    document.change_works(work_list)
    document.render(args.res_filename)
    # for w in work_list:
    #     print(w)


def print_works_from_docx():
    parser = argparse.ArgumentParser(description='Parser')
    parser.add_argument('--filename', dest='filename', required=True)
    parser.add_argument('--rdpx-filename', dest='rdpx_filename', required=True)
    parser.add_argument('--res-filename', dest='res_filename', required=True)
    args = parser.parse_args()
    play(args.filename)


def main():
    parser = argparse.ArgumentParser(description='Parser')
    parser.add_argument('--filename', dest='filename', required=True)
    parser.add_argument('--out_filename', dest='out_filename', required=True)
    args = parser.parse_args()
    template = etree.parse(args.filename)


if __name__ == "__main__":
    # main()
    # docx_main()
    # docx_const_works()
    works_from_docx()
