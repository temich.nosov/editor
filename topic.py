from dataclasses import dataclass


@dataclass
class WorkItem:
    title: str
    hours: float
    semester: int
    comments: str


@dataclass
class Topic:
    lections: "list[WorkItem]"
    lab_works: "list[WorkItem]"
    seminars: "list[WorkItem]"
    individual_works: "list[WorkItem]"
    control_works: "list[WorkItem]"
    course_works: "list[WorkItem]"
    course_projects: "list[WorkItem]"
    exams: "list[WorkItem]"
    tests: "list[WorkItem]"
    contact_hours: "list[WorkItem]"