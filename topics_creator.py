from topic import WorkItem, Topic
from work import Work
from rdpx_creator import ITEM_CODE, INDIVIDUAL, LECTURE, LAB_WORK
from rdpx_creator import PRACTICE, COURSE_WORK, COURSE_PROJ, CONTACT_WORK
from rdpx_creator import CONTROL_WORK, TEST, EXAM
from str_utils import upper_and_rest


# Создает список строк для RdpxCreator
def create_works_from_topics(topics:"list[Topic]"):
    works = []
    title = ''
    semester = '0'
    for t in [t1 for t1 in topics if len(t1.tests) == 0 and len(t1.exams) == 0]:
        if len(t.lections) > 0:
            parts = upper_and_rest(t.lections[0].title)
            title = parts[0]
            semester = str(t.lections[0].semester)
            if len(parts[1]) > 0:
                t.lections[0].title = parts[1]
            # ?!
            works.append(Work(-1, ITEM_CODE, ITEM_CODE, title, semester, 0, '', 0))
        num = 2
        for l in t.lections:
            works.append(Work(-1, 1, LECTURE, l.title, semester, l.hours, l.comments, 0, num))
            num += 1
        for l in t.seminars:
            works.append(Work(-1, 1, PRACTICE, l.title, semester, l.hours, l.comments, 0, num))
            num += 1
        for l in t.lab_works:
            works.append(Work(-1, 1, LAB_WORK, l.title, semester, l.hours, l.comments, 0, num))
            num += 1
        for l in t.individual_works:
            works.append(Work(-1, 1, INDIVIDUAL, l.title, semester, l.hours, l.comments, 0, num))
            num += 1
        #TODO: Добавить другие виды работ - частично сделано
        for l in t.control_works:
            works.append(Work(-1, ITEM_CODE, ITEM_CODE, l.title, semester, 0, '', 0))
            if l.title.find("онтрольная") > 0:
                works.append(Work(-1, 1, CONTROL_WORK, l.title, semester, l.hours, l.comments, 0, num))
            if l.title.find("урсовая") > 0:
                works.append(Work(-1, 1, COURSE_WORK, l.title, semester, l.hours, l.comments, 0, num))
            if l.title.find("урсовой") > 0:
                works.append(Work(-1, 1, COURSE_PROJ, l.title, semester, l.hours, l.comments, 0, num))
            num += 1
    #TODO и 2 раздел - пока только итоговый контроль
    test_topics = [t for t in topics if len(t.tests) > 0]
    exam_topics = [t for t in topics if len(t.exams) > 0]

    num = 1
    # if len(test_topics) > 0 or len(exam_topics) > 0:
    #     works.append(Work(-2, -1, ITEM_CODE, "Промежуточная аттестация", semester, 0, "", 0, num))
    #     num += 1

    if len(test_topics) > 0:
        for t in test_topics:
            works.append(Work(-2, -1, ITEM_CODE, "Зачет", semester, 0, "", 0, num))
            num += 1
            works.append(Work(-2, 1, TEST, "Зачет", semester, 0, "З", 0, num))
            num += 1
            works.append(Work(-2, 1, CONTACT_WORK, "Контактная работа с ППС", semester, 0.25, "З", 0, num))
            num += 1

    if len(exam_topics) > 0:
        for t in exam_topics:
            works.append(Work(-2, -1, ITEM_CODE, "Экзамен", semester, 0, "", 0, num))
            num += 1
            works.append(Work(-2, 1, EXAM, "Подготовка к экзамену", semester, 35.65, "Э", 0, num))
            num += 1
            works.append(Work(-2, 1, CONTACT_WORK, "Контактная работа с ППС", semester, 0.35, "Э", 0, num))
            num += 1

    #Обработка семестров ! - СДЕЛАНО !
    return works
