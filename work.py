# Переделать в dataclass ?
from dataclasses import dataclass


# class Work:
#     def __init__(self, part, type, rup_type, name, semester, hours=0, comment='', parent=0):
#         self.part = part
#         self.type = type
#         self.rup_type = rup_type
#         self.name = name
#         self.semester = semester
#         self.hours = hours
#         self.comment = comment
#         self.parent = parent


@dataclass
class Work:
    part: int
    type: int
    rup_type: int
    name: str
    semester: str
    hours: float = 0.0
    comment: str = ''
    parent: int = 0
    order: int = 0
